<?php
namespace Drupal\biopama_documents\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_documents module.
 */
class BiopamaDocumentsController extends ControllerBase{
  public function content() {
    $element = array(
      '#theme' => 'page-documents',
    );
    return $element;
  }
}